import math
import matplotlib.pyplot as plt


def main():
    """Главный метод программы"""

    while True:
        processor()
        if input('Для продолжения введите "y", для выхода "n"') == "n":
            break


def processor():
    """Функция ввода/вычисления/вывода программы"""

    (a, x1, x2, dx) = get_user_input()
    x = x1
    results_g = []
    results_f = []
    results_y = []
    x_vals = []
    while x <= x2:
        results_g.append(calc_g(a, x))
        results_f.append(calc_f(a, x))
        results_y.append(calc_y(a, x))
        x_vals.append(x)
        x += dx
        if x > x2:
            break

    save_to_file("Data_G.txt", results_g)
    save_to_file("Data_F.txt", results_f)
    save_to_file("Data_Y.txt", results_y)

    results_g.clear()
    results_f.clear()
    results_y.clear()

    results_g = read_from_file("Data_G.txt")
    results_f = read_from_file("Data_F.txt")
    results_y = read_from_file("Data_Y.txt")

    show_chart(x_vals, results_g)
    show_chart(x_vals, results_f)
    show_chart(x_vals, results_y)


def get_user_input():
    """Функция пользовательского ввода"""

    string = input('Введите a: ')
    a = 0
    try:
        a = float(string)
    except ValueError:
        print('Это не число')
        exit()

    string = input('Введите x1: ')
    x1 = 0
    try:
        x1 = float(string)
    except ValueError:
        print('Это не число')
        exit()

    string = input('Введите x2: ')
    x2 = 0
    try:
        x2 = float(string)
    except ValueError:
        print('Это не число')
        exit()

    string = input('Введите dx: ')
    dx = 0
    try:
        dx = float(string)
    except ValueError:
        print('Это не число')
        exit()

    return a, x1, x2, dx


def calc_g(a, x):
    """Функция подсчета формулы G"""

    try:
        return (10 * (-45 * a ** 2 + 49 * a * x + 6 * x ** 2)) / (
                15 * a ** 2 + 49 * a * x + 24 * x ** 2)
    except ZeroDivisionError:
        return 'Попытка деления на ноль'


def calc_f(a, x):
    """Функция подсчета формулы F"""

    return math.tan(math.radians(5 * a ** 2 + 34 * a * x + 45 * x ** 2))


def calc_y(a, x):
    """Функция подсчета формулы Y"""

    try:
        return -1 * math.asin(math.radians(7 * a ** 2 - a * x - 8 * x ** 2))
    except ValueError:
        return 'Ошибка'


def show_chart(x_vals, results):
    """Функция генерации графика"""

    width = 0.35

    fig, ax = plt.subplots()

    ax.plot(x_vals, results, width)

    ax.set_ylabel('Results')
    ax.set_xlabel('X')
    ax.set_title('Function')

    plt.show()


def save_to_file(file_name, results):
    """Запись массива в файл"""

    with open(file_name, "w") as file:
        for result in results:
            file.write(str(result) + '\n')


def read_from_file(file_name):
    """Чтение данных из файла в массив"""
    results = []
    with open(file_name, "r") as file:
        for result in file:
            print(result)
            results.append(result)
    return results


main()
